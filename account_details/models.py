from django.db import models
from django.db.models.signals import pre_save

from accounts.models import Account
from backend.helpers import *

import json


# Create your models here.
DEFAULT_PERMISSIONS = {
        'loan': {
            'create': False,
            'retrieve': False,
            'update': False,
            'delete': False
        },
        'transaction': {
            'create': False,
            'retrieve': False,
            'update': False,
            'delete': False
        },
        'account': {
            'create': False,
            'retrieve': False,
            'update': False,
            'delete': False
        },
    }


class AccountDetail(models.Model):
    account = models.OneToOneField(Account, on_delete=models.CASCADE)
    full_name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=50)
    permissions = models.TextField(null=True, blank=True, default=json.dumps(DEFAULT_PERMISSIONS),)
    permanent_address = models.CharField(max_length=100)
    temporary_address = models.CharField(max_length=100)
    dob = models.CharField(max_length=10)
    recruit_date = models.CharField(max_length=10)
    recruit_position = models.CharField(max_length=10)
    recruit_level = models.CharField(max_length=100)
    current_date = models.CharField(max_length=10)
    current_position = models.CharField(max_length=100)
    current_level = models.CharField(max_length=100)
    citizenship_no = models.CharField(max_length=30)
    citizenship_issued_place = models.CharField(max_length=100)
    citizenship_issued_date = models.CharField(max_length=10)
    service_break = models.CharField(max_length=100, null=True, blank=True)
    retirement_date = models.CharField(max_length=10)
    vacations = models.CharField(max_length=100, null=True, blank=True)
    created_at = models.CharField(max_length=10, null=True, blank=True)
    updated_at = models.CharField(max_length=10, null=True, blank=True)

    def __str__(self):
        return "{}'s details, ID-{}".format(self.full_name, self.employee_id)


def pre_save_change(sender, instance, *args, **kwargs):
    # checking the value of created-at
    # if the value is true, it means the instance is getting modified,
    # so, we don't need to handle created_at field.
    if not instance.created_at:
        instance.created_at = english_to_nepali()

    instance.updated_at = english_to_nepali()


pre_save.connect(pre_save_change, AccountDetail)
