import graphene
from graphene_django import DjangoObjectType
from .models import AccountDetail


class AccountDetailType(DjangoObjectType):
    class Meta:
        model = AccountDetail
