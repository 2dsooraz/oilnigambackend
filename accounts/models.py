from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models.signals import pre_save

import json

from backend.helpers import *


# Create your models here.
class Account(AbstractUser):
    REQUIRED_FIELDS = ['is_staff', 'email', ]
    created_at = models.CharField(max_length=10, null=True, blank=True)
    updated_at = models.CharField(max_length=10, null=True, blank=True)

    def __str__(self):
        return '{}'.format(self.username)


def pre_save_change(sender, instance, *args, **kwargs):
    # checking the value of created-at
    # if the value is true, it means the instance is getting modified,
    # so, we don't need to handle created_at field.
    if not instance.created_at:
        instance.created_at = english_to_nepali()

    instance.updated_at = english_to_nepali()


pre_save.connect(pre_save_change, Account)
