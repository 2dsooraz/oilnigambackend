import graphene
from graphene_django import DjangoObjectType
from django.db.models import Q
from django.db.utils import IntegrityError
from graphql_jwt.decorators import user_passes_test

from .models import Account
from account_details.models import AccountDetail
from account_details.schema import AccountDetailType
from backend.utils import *


# Accounttype is a graphene model linking graphql and django model.
# It handles on single instance of model defined in its meta.
class AccountType(DjangoObjectType):
    class Meta:
        model = Account


# This class is for contolling the pages for pagination.
# It handles on list of instances.
class AccountPageType(graphene.ObjectType):
    data = graphene.List(AccountType)
    rows = graphene.Int()
    rows_count = graphene.Int()
    page = graphene.Int()
    pages = graphene.Int()


class Query(graphene.ObjectType):
    all_accounts = graphene.Field(AccountPageType,
                                  search=graphene.String(),
                                  is_staff=graphene.Boolean(),
                                  page=graphene.Int(),
                                  rows=graphene.Int(),
                                  order=graphene.String(description="Allowed values: 'pk', '-pk', 'username', '-username'"), default_value='')
    account = graphene.Field(AccountType,
                             username=graphene.String(),
                             )

    @user_passes_test(lambda user: check_user_permissions(user, 'account', 'retrieve'))
    def resolve_all_accounts(self, info, page=1, rows=10, **kwargs):
        filters = Q()

        if kwargs.get("search"):
            filters = Q(username__icontains=kwargs.get("search")) | Q(email__icontains=kwargs.get("search"))

        if kwargs.get("is_staff") is not None:
            filters = filters & Q(is_staff=kwargs.get("is_staff"))

        data, rows_count, rows, pages, page = paginate(Account, page, rows, filters, order=kwargs.get('order'))
        return AccountPageType(
            data=data,
            rows=rows,
            rows_count=rows_count,
            page=page,
            pages=pages
        )

    @user_passes_test(lambda user: check_user_permissions(user, 'account', 'retrieve'))
    def resolve_account(self, info, **kwargs):
        if kwargs.get("username"):
            return Account.objects.get(username=kwargs.get("username"))
        return None


class CreateAccount(graphene.Mutation):
    # these field are returned by this mutation.
    # variable = datatype(modeltype)
    account = graphene.Field(AccountType)
    account_detail = graphene.Field(AccountDetailType)
    status = graphene.Int()
    msg = graphene.String()

    class Arguments:
        username = graphene.String(required=True)
        password = graphene.String(required=True)
        is_staff = graphene.Boolean(default_value=False)
        email = graphene.String(required=True)
        full_name = graphene.String(required=True)
        employee_id = graphene.String(required=True)
        permissions = graphene.String()
        permanent_address = graphene.String(required=True)
        temporary_address = graphene.String(required=True)
        dob = graphene.String(required=True)
        recruit_date = graphene.String(required=True)
        recruit_position = graphene.String(required=True)
        recruit_level = graphene.String(required=True)
        current_position = graphene.String(required=True)
        current_level = graphene.String(required=True)
        citizenship_no = graphene.String(required=True)
        citizenship_issued_place = graphene.String(required=True)
        citizenship_issued_date = graphene.String(required=True)
        service_break = graphene.String()
        vacations = graphene.String()

    @user_passes_test(lambda user: check_user_permissions(user, 'account', 'create'))
    def mutate(self, info, *args, **kwargs):
        middle_flag = False
        successful_flag = False
        try:
            new_account = Account.objects.create(
                username=kwargs.get("username"),
                email=kwargs.get("email"),
                is_staff=kwargs.get("is_staff"),
            )
            new_account.set_password(kwargs.get("password"))
            new_account.save()
            middle_flag = True
            # raise Exception("MANUALLY RAISED")
            new_account_detail = AccountDetail.objects.create(
                full_name=kwargs.get("full_name"),
                employee_id=kwargs.get("employee_id"),
                permissions=kwargs.get("permission"),
                permanent_address=kwargs.get("permanent_address"),
                temporary_address=kwargs.get("temporary_address"),
                dob=kwargs.get("dob"),
                recruit_date=kwargs.get("recruit_date"),
                recruit_position=kwargs.get("recruit_position"),
                recruit_level=kwargs.get("recruit_level"),
                current_position=kwargs.get("current_position"),
                current_level=kwargs.get("current_level"),
                citizenship_no=kwargs.get("citizenship_no"),
                citizenship_issued_place=kwargs.get("citizenship_issued_place"),
                citizenship_issued_date=kwargs.get("citizenship_issued_date"),
                service_break=kwargs.get("service_break"),
                vacations=kwargs.get("vacations"),
                account=new_account
            )
            new_account_detail.save()
            successful_flag = True
            return CreateAccount(
                account=new_account,
                account_detail=new_account_detail,
                msg="Successfully Created",
                status=200,
            )
        except IntegrityError as e:
            msg = "{}".format(e)
            status = 400
        except Exception as e:
            msg = "{}".format(e)
            status = 500
        finally:
            if not successful_flag:
                if middle_flag:
                    new_account.delete()
                return CreateAccount(
                    msg=msg,
                    status=status
                )


# edit account mutation
class EditAccount(graphene.Mutation):
    # these field are returned by this mutation.
    # variable = datatype(modeltype)
    account = graphene.Field(AccountType)
    account_detail = graphene.Field(AccountDetailType)
    status = graphene.Int()
    msg = graphene.String()

    class Arguments:
        is_staff = graphene.Boolean(default_value=False)
        email = graphene.String(required=True)
        full_name = graphene.String(required=True)
        employee_id = graphene.String(required=True)
        permissions = graphene.String()
        permanent_address = graphene.String(required=True)
        temporary_address = graphene.String(required=True)
        dob = graphene.String(required=True)
        recruit_date = graphene.String(required=True)
        recruit_position = graphene.String(required=True)
        recruit_level = graphene.String(required=True)
        current_position = graphene.String(required=True)
        current_level = graphene.String(required=True)
        citizenship_no = graphene.String(required=True)
        citizenship_issued_place = graphene.String(required=True)
        citizenship_issued_date = graphene.String(required=True)
        service_break = graphene.String()
        vacations = graphene.String()

    @user_passes_test(lambda user: check_user_permissions(user, 'account', 'update'))
    def mutate(self, info, *args, **kwargs):
        middle_flag = False
        successful_flag = False
        try:
            new_account = Account.objects.create(
                email=kwargs.get("email"),
                is_staff=kwargs.get("is_staff"),
            )
            new_account.save()
            middle_flag = True
            # raise Exception("MANUALLY RAISED")
            new_account_detail = AccountDetail.objects.create(
                full_name=kwargs.get("full_name"),
                employee_id=kwargs.get("employee_id"),
                permissions=kwargs.get("permission"),
                permanent_address=kwargs.get("permanent_address"),
                temporary_address=kwargs.get("temporary_address"),
                dob=kwargs.get("dob"),
                recruit_date=kwargs.get("recruit_date"),
                recruit_position=kwargs.get("recruit_position"),
                recruit_level=kwargs.get("recruit_level"),
                current_position=kwargs.get("current_position"),
                current_level=kwargs.get("current_level"),
                citizenship_no=kwargs.get("citizenship_no"),
                citizenship_issued_place=kwargs.get("citizenship_issued_place"),
                citizenship_issued_date=kwargs.get("citizenship_issued_date"),
                service_break=kwargs.get("service_break"),
                vacations=kwargs.get("vacations"),
                account=new_account
            )
            new_account_detail.save()
            successful_flag = True
            return EditAccount(
                account=new_account,
                account_detail=new_account_detail,
                msg="Successfully Created",
                status=200,
            )
        except IntegrityError as e:
            msg = "{}".format(e)
            status = 400
        except Exception as e:
            msg = "{}".format(e)
            status = 500
        finally:
            if not successful_flag:
                if middle_flag:
                    new_account.delete()
                return EditAccount(
                    msg=msg,
                    status=status
                )


class Mutation(graphene.ObjectType):
    create_account = CreateAccount.Field()
    edit_account = EditAccount.Field()
