from math import ceil
from nepali_date import NepaliDate

import pandas as pd


def get_nepali_date(date=None):
    if date is None:
        return NepaliDate.today().strfdate("%Y-%m-%d")

    else:
        return NepaliDate.strpdate(date, '%Y-%m-%d')


def english_to_nepali(english_date=None):
    if english_date is None:
        return NepaliDate.today().strfdate("%Y-%m-%d")
    else:
        return NepaliDate.to_nepali_date(english_date).strfdate("%Y-%m-%d")


def convert_to_present_value(past_details):
    rate = ((1 + past_details.get('rate') / 100) ** (1 / 12)) - 1
    present_value = float(past_details.get('principle')) * (1 + rate / 12) ** (12 * past_details.get('time'))
    return present_value


class LoanCalculator:
    def __init__(self, loan_amount, interest_rate, loan_period_yrs, num_payments_per_year, start_date):
        self.loan_amount = round(float(loan_amount), 2)
        self.interest_rate = round(float(interest_rate), 2)
        self.loan_period_yrs = round(float(loan_period_yrs), 2)
        self.num_payments_per_year = round(float(num_payments_per_year), 2)
        self.start_date = start_date
        self.monthly_increment = 12 / self.num_payments_per_year
        self.number_of_payments = ceil(self.loan_period_yrs * self.num_payments_per_year)
        self.effective_interest_rate = self.interest_rate / 100 / self.num_payments_per_year
        self.payment_per_period = 0 if self.number_of_payments == 0 else self.calculate_pmt()

    def calculate_pmt(self):
        pmt = self.loan_amount * self.effective_interest_rate / (1 - (1 + self.effective_interest_rate) ** (-self.number_of_payments))

        return round(pmt, 2)

    def generate_table(self):
        df = pd.DataFrame(columns=[
            "payment_no",
            "payment_date",
            "payment",
            "principal",
            "interest",
            # "extra_payments",
            "balance"
        ])

        year, month = self.start_date.year, self.start_date.month
        date_format = "{}-{}"
        for i in range(1, self.number_of_payments + 1):
            if month > 12:
                carry_over = month // 12
                month = month % 12
                year += carry_over

            if i == 1:
                interest = round(self.loan_amount * self.effective_interest_rate, 2)
                new_row = {'payment_no': i, 'payment_date': date_format.format(int(year), int(month)), 'payment': self.payment_per_period,
                           'principal': self.payment_per_period - interest, 'interest': interest,
                           # 'extra_payments': interest,
                           'balance': self.loan_amount - self.payment_per_period
                           }

                df = df.append(new_row, ignore_index=True)
            else:

                prev_balance = df.loc[i-2]['balance']
                if i >= self.number_of_payments or prev_balance <= 0:
                    break

                interest = prev_balance * self.effective_interest_rate
                # payment_due_at = self.start_date + dt.timedelta(days=counter*30)
                payment_due_at = date_format.format(int(year), int(month))
                payment = self.payment_per_period if self.effective_interest_rate + interest < prev_balance else prev_balance + interest
                principal = payment - interest
                # extra_payments = prev_extra_payments + interest
                new_balance = prev_balance - principal # - extra_payments
                new_row = {
                    'payment_no': i, 'payment_date': payment_due_at, 'payment': payment,
                    'principal': principal, 'interest': interest, #'extra_payments': extra_payments,
                    'balance': new_balance
                }

                df = df.append(new_row, ignore_index=True)
            month += self.monthly_increment
        df = df.round(decimals=2)
        return df.to_dict('records')


# :TODO: Remove in Production
# if __name__ == '__main__':
#     from nepali_date import NepaliDate
#     some_date = NepaliDate.strpdate('2071-01-01', '%Y-%m-%d')
#     lc = LoanCalculator(4356000, 2, 20.3, 12, some_date)
#     lc.generate_table()
