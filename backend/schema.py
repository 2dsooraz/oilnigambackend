# import json
#
# import graphene
#
# from django.core.exceptions import ObjectDoesNotExist
# from loans.models import Loan, LoanType as LT
# from payments.models import Payment
# import datetime as dt
#
# from loans.schema import LoanType
# from payments.schema import PaymentType
# from .helpers import *
#
#
# class LoanDetailType(graphene.ObjectType):
#     loan_detail = graphene.Field(LoanType)
#     breakdown = graphene.String()
#     payment_details = graphene.List(PaymentType)
#     msg = graphene.String()
#     status = graphene.Int()
#
#
# class AnalyticsType(graphene.ObjectType):
#     msg = graphene.String()
#     status = graphene.Int()
#     pie_chart = graphene.String()
#     line_chart = graphene.String()
#     bar_chart = graphene.String()
#     forecast_chart = graphene.String()
#
#
# class Query(graphene.ObjectType):
#     loan_details = graphene.Field(LoanDetailType,
#                                   loan_type=graphene.String(required=True),
#                                   dob=graphene.String(required=True),
#                                   loan_amount=graphene.Float(required=True)
#                                   )
#
#     analytics = graphene.Field(AnalyticsType)
#
#     def resolve_loan_details(self, info, *args, **kwargs):
#         try:
#             loan_type = kwargs.get('loan_type')
#             dob = kwargs.get('dob')
#             loan_amount = kwargs.get('loan_amount')
#
#             loan = Loan.objects.get(loan_type__name=loan_type, loan_amount=loan_amount, loan_of__accountdetail__dob__exact=dob)
#             retirement_date = get_nepali_date(date=loan.loan_of.accountdetail.retirement_date)
#             loan_start_date = get_nepali_date(date=loan.loan_commence_date)
#
#             if loan_start_date is None:
#                 return LoanDetailType(
#                     msg='Success',
#                     status=200,
#                     loan_detail=loan,
#                 )
#
#             today_date = NepaliDate.today()
#             elapsed_time = (today_date.to_english_date() - loan_start_date.to_english_date()).days / 365
#             period_years = loan.loan_type.period_years - elapsed_time
#
#             if today_date + dt.timedelta(weeks=period_years * 52) > retirement_date:
#                 period_years = (retirement_date.to_english_date() - today_date.to_english_date()).days / 365
#
#             rate = loan.loan_type.interest_rate
#
#             if not loan.loan_issue_1_status:
#                 breakdown = None
#                 issues_present_val = 0
#             else:
#                 issue_1_amount = loan.loan_issue_1_amount
#                 issue_1_date = get_nepali_date(date=loan.loan_issue_1_date)
#                 issue_1_period = (today_date.to_english_date() - issue_1_date.to_english_date()).days / 365
#                 issue_1_present_val = convert_to_present_value({'rate': rate, 'principle': issue_1_amount, 'time': issue_1_period})
#
#                 if loan.loan_issue_2_status:
#                     issue_2_amount = loan.loan_issue_2_amount
#                     issue_2_date = get_nepali_date(date=loan.loan_issue_2_date)
#                     issue_2_period = (today_date.to_english_date() - issue_2_date.to_english_date()).days / 365
#                     issue_2_present_val = convert_to_present_value({'rate': rate, 'principle': issue_2_amount, 'time': issue_2_period})
#                     issues_present_val = issue_1_present_val + issue_2_present_val
#                 else:
#                     issues_present_val = issue_1_present_val
#
#             payments = loan.payment_set.all().order_by('-updated_at')
#
#             payments_present_val = 0
#             for payment in payments:
#                 payment_amount = payment.payment_amount
#                 payment_date = get_nepali_date(payment.payment_date)
#                 payment_period = (today_date.to_english_date() - payment_date.to_english_date()).days / 365
#                 payment_present_val = convert_to_present_value({'rate': rate, 'principle': payment_amount, 'time': payment_period})
#                 payments_present_val += payment_present_val
#
#             new_loan_amount = issues_present_val - payments_present_val
#             lc = LoanCalculator(new_loan_amount, rate, period_years, loan.loan_type.num_payments_per_year, loan_start_date)
#             breakdown = lc.generate_table()
#             breakdown = json.dumps(breakdown)
#             return LoanDetailType(
#                 msg='Success',
#                 loan_detail=loan,
#                 breakdown=breakdown,
#                 payment_details=payments,
#                 status=200
#             )
#         except ObjectDoesNotExist:
#             return LoanDetailType(
#                 msg='Loan not found',
#                 status=404
#             )
#         except Exception as e:
#             return LoanDetailType(
#                 # msg='Something went wrong',
#                 msg=str(e),
#                 status=500,
#             )
#
#     def resolve_analytics(self, info, *args, **kwargs):
#         try:
#             today = NepaliDate.today()
#             current_year = today.year
#             current_month = today.month
#             bar_chart_data = []
#             line_chart_data = []
#             pie_chart_data = []
#
#             if current_month == 12:
#                 start_year = current_year
#                 start_month = 1
#                 check_at = [(start_year, 1) for i in range(1, current_month + 1)]
#             else:
#                 start_year = current_year - 1
#                 start_month = current_month + 1
#                 check_at = [(start_year, i) for i in range(start_month, 13)]
#                 check_at.extend([(current_year, i) for i in range(1, current_month + 1)])
#
#             # loan_types = [i[0] for i in Loan.objects.all().values_list('loan_type').distinct()]
#             loan_types = [l.name for l in LT.objects.all()]
#
#             for loan_type in loan_types:
#                 payment_collection_this_month = 0
#                 loan_issued_this_month = 0
#                 loans = Loan.objects.filter(loan_type__name__exact='loan_type')
#                 for loan in loans:
#                     loan_issue_1_date = get_nepali_date(date=loan.loan_issue_1_date)
#                     if loan.loan_issue_1_status and loan_issue_1_date.year == current_year and loan_issue_1_date.month == current_month:
#                         loan_issued_this_month += loan.loan_issue_1_amount
#                     loan_issue_2_date = get_nepali_date(date=loan.loan_issue_2_date)
#                     if loan.loan_issue_2_status and loan_issue_2_date.year == current_year and loan_issue_2_date.month == current_month:
#                         loan_issued_this_month += loan.loan_issue_2_amount
#
#                     payments = loan.payment_set.all()
#                     for payment in payments:
#                         payment_date = get_nepali_date(date=payment.payment_date)
#                         if payment_date.year == current_year and payment_date.month == current_month:
#                             payment_collection_this_month += payment.payment_amount
#
#                 # :TODO: forecast chart remains
#                 bar_chart_data.append({'name': loan_type, 'loan_issued_this_month': loan_issued_this_month})
#                 pie_chart_data.append({'name': loan_type, 'value': payment_collection_this_month})
#
#             # for check in check_at:
#             for check in check_at:
#                 month_name = '{}-{}'.format(check[0], check[1])
#                 final_json = {'name': month_name}
#                 for loan_type in loan_types:
#                     payments = Payment.objects.filter(payment_of__loan_type__exact=loan_type, payment_date__istartswith=str(check[0])+'-', payment_date__icontains='-'+str(check[1])+'-')
#                     total_payment_for_checked_month = 0
#                     for payment in payments:
#                         total_payment_for_checked_month += payment.payment_amount
#
#                     final_json.update({loan_type: total_payment_for_checked_month})
#
#                 line_chart_data.append(final_json)
#
#             return AnalyticsType(
#                 msg='Success',
#                 status=200,
#                 line_chart=json.dumps(line_chart_data),
#                 pie_chart=json.dumps(pie_chart_data),
#                 bar_chart=json.dumps(bar_chart_data)
#             )
#         except Exception as e:
#             return AnalyticsType(
#                 msg='Something went wrong.',
#                 status=500,
#             )
