import base64
import math
from django.core.files.base import ContentFile
import json


def convert_base64_to_image(encoded_string, file_name):
    img_format, str_img = encoded_string.split(";base64,")
    extension = img_format.split('/')[-1]
    img_file = ContentFile(base64.b64decode(str_img), name=file_name+'.'+extension)
    return img_file


def paginate(Klass, page, rows, filters=None, order=''):
    print(Klass, page, rows)

    res = Klass.objects.all()

    if filters is not None:
        res = res.filter(filters)

    if order != '':
        res = res.order_by(order)

    row_count = res.count()

    if row_count < rows:
        rows = row_count

    pages = math.ceil(row_count / rows) if rows != 0 else 1

    if page > pages:
        page = 1
        offset = 0
    else:
        offset = (page - 1) * rows

    return res[offset:][:rows], row_count, rows, pages, page


def check_user_permissions(user, obj_type, action):
    try:
        perms = json.loads(user.accountdetail.permissions)
        has_perm = perms.get(obj_type, {}).get(action, False)
        return has_perm
    except Exception as e:
        return False
