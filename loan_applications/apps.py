from django.apps import AppConfig


class LoanApplicationsConfig(AppConfig):
    name = 'loan_applications'
