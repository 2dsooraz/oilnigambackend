from django.db import models
from django.db.models.signals import pre_save

from accounts.models import Account
from backend.helpers import *
from loan_types.models import LoanType


# Create your models here.
def get_loan_assets_path(instance, filename):
    return 'loan_applications/{}/{}'.format(instance.pk, filename)


class LoanApplication(models.Model):
    STATUS_CHOICES = (
        ('PENDING', 'PENDING'),
        ('REJECTED', 'REJECTED'),
        ('APPROVED', 'APPROVED'),
    )
    loan_type = models.ForeignKey(LoanType, on_delete=models.DO_NOTHING)
    loan_amount = models.FloatField(blank=False, null=False)
    loan_for = models.CharField(max_length=100)
    status = models.CharField(max_length=20, choices=STATUS_CHOICES)
    loan_utilization_place = models.CharField(max_length=100)
    submission_form = models.ImageField(upload_to=get_loan_assets_path, null=True, blank=True)
    lalpurja = models.ImageField(upload_to=get_loan_assets_path, null=True, blank=True)
    malpot_receipt_1 = models.ImageField(upload_to=get_loan_assets_path, null=True, blank=True)
    malpot_receipt_2 = models.ImageField(upload_to=get_loan_assets_path, null=True, blank=True)
    verified_map = models.ImageField(upload_to=get_loan_assets_path, null=True, blank=True)
    blueprint = models.ImageField(upload_to=get_loan_assets_path, null=True, blank=True)
    committee_sifaris = models.ImageField(upload_to=get_loan_assets_path, null=True, blank=True)
    marriage_certificate = models.ImageField(upload_to=get_loan_assets_path, null=True, blank=True)
    mun_vdc_sifaris = models.ImageField(upload_to=get_loan_assets_path, null=True, blank=True)
    dristibhandha = models.ImageField(upload_to=get_loan_assets_path, null=True, blank=True)
    close_house_photo = models.ImageField(upload_to=get_loan_assets_path, null=True, blank=True)
    inspection_report = models.ImageField(upload_to=get_loan_assets_path, null=True, blank=True)
    anusuchi_six = models.ImageField(upload_to=get_loan_assets_path, null=True, blank=True)
    tippani = models.ImageField(upload_to=get_loan_assets_path, null=True, blank=True)
    voucher = models.ImageField(upload_to=get_loan_assets_path, null=True, blank=True)
    debit_credit = models.ImageField(upload_to=get_loan_assets_path, null=True, blank=True)
    quotation = models.ImageField(upload_to=get_loan_assets_path, null=True, blank=True)
    memo = models.ImageField(upload_to=get_loan_assets_path, null=True, blank=True)
    credit_note = models.ImageField(upload_to=get_loan_assets_path, null=True, blank=True)
    approved_letter = models.ImageField(upload_to=get_loan_assets_path, null=True, blank=True)
    full_name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=50)
    permanent_address = models.CharField(max_length=100)
    temporary_address = models.CharField(max_length=100)
    dob = models.CharField(max_length=10)
    recruit_date = models.CharField(max_length=10)
    recruit_position = models.CharField(max_length=10)
    recruit_level = models.CharField(max_length=100)
    current_position = models.CharField(max_length=100)
    current_level = models.CharField(max_length=100)
    current_branch = models.CharField(max_length=100)
    citizenship_no = models.CharField(max_length=30)
    citizenship_issued_place = models.CharField(max_length=100)
    citizenship_issued_date = models.CharField(max_length=10)
    service_break = models.CharField(max_length=100, null=True, blank=True)
    retirement_date = models.CharField(max_length=10)
    vacations = models.CharField(max_length=100, null=True, blank=True)
    comment = models.TextField(null=True, blank=True)
    created_at = models.CharField(max_length=10, null=True, blank=True)
    updated_at = models.CharField(max_length=10, null=True, blank=True)

    def __str__(self):
        return "{}'s {} loan applications".format(self.full_name, self.loan_type.name)


def pre_save_change(sender, instance, *args, **kwargs):
    # checking the value of created-at
    # if the value is true, it means the instance is getting modified,
    # so, we don't need to handle created_at field.
    if not instance.created_at:
        instance.created_at = english_to_nepali()

    instance.updated_at = english_to_nepali()


pre_save.connect(pre_save_change, LoanApplication)