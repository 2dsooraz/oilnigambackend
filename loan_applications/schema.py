import graphene
from graphene_django import DjangoObjectType

from django.db.models import Q
from django.core.exceptions import ObjectDoesNotExist
from graphql_jwt.decorators import user_passes_test

from .models import LoanApplication
from loan_types.models import LoanType
from backend.utils import *


class LoanApplicationType(DjangoObjectType):
    class Meta:
        model = LoanApplication


class LoanApplicationPageType(graphene.ObjectType):
    data = graphene.List(LoanApplicationType)
    rows = graphene.Int()
    rows_count = graphene.Int()
    page = graphene.Int()
    pages = graphene.Int()


class Query(graphene.ObjectType):
    all_loan_applications = graphene.Field(LoanApplicationPageType,
                               search=graphene.String(),
                               emp_id=graphene.String(),
                               page=graphene.Int(),
                               rows=graphene.Int(),
                               order=graphene.String(description="Allowed Values: 'pk', '-pk', 'employee_id', '-employee_id' etc.", default_value=''))
    loan_application = graphene.Field(LoanApplicationType,
                            loan_application_id=graphene.Int()
                          )

    @user_passes_test(lambda user: check_user_permissions(user, 'loan', 'retrieve'))
    def resolve_all_loan_applications(self, info, page=1, rows=10, *args, **kwargs):
        filters = Q()

        if kwargs.get('search'):
            filters = Q(loan_type__name__icontains=kwargs.get('search')) | Q(permanent_address__icontains = kwargs.get('search')) | Q(temporary_address__icontains=kwargs.get('search')) | Q(current_position__icontains=kwargs.get('search')) | Q(status__icontains=kwargs.get('search'))
        if kwargs.get('emp_id'):
            filters = filters & Q(employee_id__icontains=kwargs.get('emp_id'))

        data, rows_count, rows, pages, page = paginate(LoanApplication, page, rows, filters, order=kwargs.get('order'))
        return LoanApplicationPageType(
            data=data,
            rows=rows,
            rows_count=rows_count,
            page=page,
            pages=pages,
        )

    @user_passes_test(lambda user: check_user_permissions(user, 'loan', 'retrieve'))
    def resolve_loan_application(self, info, **kwargs):
        if kwargs.get('loan_application_id'):
            return LoanApplication.objects.get(pk=kwargs.get('loan_application_id'))
        return None


class CreateLoanApplication(graphene.Mutation):
    loan_application = graphene.Field(LoanApplicationType)
    status = graphene.Int()
    msg = graphene.String()

    class Arguments:
        loan_type = graphene.Int(required=True)
        loan_amount = graphene.Float(required=True)
        loan_for = graphene.String(required=True)
        status = graphene.String(default_value='PENDING')
        loan_utilization_place = graphene.String(required=True)
        submission_form = graphene.String()
        lalpurja = graphene.String()
        malpot_receipt_1 = graphene.String()
        malpot_receipt_2 = graphene.String()
        verified_map = graphene.String()
        blueprint = graphene.String()
        committee_sifaris = graphene.String()
        marriage_certificate = graphene.String()
        mun_vdc_sifaris = graphene.String()
        dristibhandha = graphene.String()
        close_house_photo = graphene.String()
        inspection_report = graphene.String()
        anusuchi_six = graphene.String()
        tippani = graphene.String()
        voucher = graphene.String()
        debit_credit = graphene.String()
        quotation = graphene.String()
        memo = graphene.String()
        credit_note = graphene.String()
        approved_letter = graphene.String()
        full_name = graphene.String(required=True)
        employee_id = graphene.String(required=True)
        permanent_address = graphene.String(required=True)
        temporary_address = graphene.String(required=True)
        dob = graphene.String(required=True)
        recruit_date = graphene.String(required=True)
        recruit_position = graphene.String(required=True)
        recruit_level = graphene.String(required=True)
        current_position = graphene.String(required=True)
        current_level = graphene.String(required=True)
        current_branch = graphene.String(required=True)
        citizenship_no = graphene.String(required= True)
        citizenship_issued_place = graphene.String(required=True)
        citizenship_issued_date = graphene.String(required=True)
        service_break = graphene.String()
        retirement_date = graphene.String(required=True)
        vacations = graphene.String()
        comment = graphene.String()

    def mutate(self, info, *args, **kwargs):
        try:
            loan_class = LoanType.objects.get(pk=kwargs.get('loan_type'))
            new_loan_application = LoanApplication.objects.create(
                loan_type=loan_class,
                loan_amount=kwargs.get('loan_amount'),
                loan_for=kwargs.get('loan_for'),
                status=kwargs.get('status'),
                loan_utilization_place=kwargs.get('loan_utilization_place'),
                full_name=kwargs.get('full_name'),
                employee_id=kwargs.get('employee_id'),
                permanent_address=kwargs.get('permanent_address'),
                temporary_address=kwargs.get('temporary_address'),
                dob=kwargs.get('dob'),
                recruit_date=kwargs.get('recruit_date'),
                recruit_position=kwargs.get('recruit_position'),
                recruit_level=kwargs.get('recruit_level'),
                current_position=kwargs.get('current_position'),
                current_level=kwargs.get('current_level'),
                current_branch=kwargs.get('current_branch'),
                citizenship_no=kwargs.get('citizenship_no'),
                citizenship_issued_place=kwargs.get('citizenship_issued_place'),
                citizenship_issued_date=kwargs.get('citizenship_issued_date'),
                retirement_date=kwargs.get('retirement_date'),
            )

            if kwargs.get('submission_form'):
                new_loan_application.submission_form = convert_base64_to_image(kwargs.get('submission_form'), 'submission_form')
            if kwargs.get('lalpurja'):
                new_loan_application.lalpurja = convert_base64_to_image(kwargs.get('lalpurja'), 'lalpurja')
            if kwargs.get('malpot_receipt_1'):
                new_loan_application.malpot_receipt_1 = convert_base64_to_image(kwargs.get('malpot_receipt_1'), 'malpot_receipt_1')
            if kwargs.get('malpot_receipt_2'):
                new_loan_application.malpot_receipt_2 = convert_base64_to_image(kwargs.get('malpot_receipt_2'), 'malpot_receipt_2')
            if kwargs.get('verified_map'):
                new_loan_application.verified_map = convert_base64_to_image(kwargs.get('verified_map'), 'verified_map')
            if kwargs.get('blueprint'):
                new_loan_application.blueprint = convert_base64_to_image(kwargs.get('blueprint'), 'blueprint')
            if kwargs.get('committee_sifaris'):
                new_loan_application.committee_sifaris = convert_base64_to_image(kwargs.get('committee_sifaris'),
                                                                     'committee_sifaris')
            if kwargs.get('marriage_certificate'):
                new_loan_application.marriage_certificate = convert_base64_to_image(kwargs.get('marriage_certificate'),
                                                                        'marriage_certificate')
            if kwargs.get('mun_vdc_sifaris'):
                new_loan_application.mun_vdc_sifaris = convert_base64_to_image(kwargs.get('mun_vdc_sifaris'), 'mun_vdc_sifaris')
            if kwargs.get('dristibhandha'):
                new_loan_application.dristibhandha = convert_base64_to_image(kwargs.get('dristibhandha'), 'dristibhandha')
            if kwargs.get('close_house_photo'):
                new_loan_application.close_house_photo = convert_base64_to_image(kwargs.get('close_house_photo'),
                                                                     'close_house_photo')
            if kwargs.get('inspection_report'):
                new_loan_application.inspection_report = convert_base64_to_image(kwargs.get('inspection_report'),
                                                                     'inspection_report')
            if kwargs.get('anusuchi_six'):
                new_loan_application.anusuchi_six = convert_base64_to_image(kwargs.get('anusuchi_six'), 'anusuchi_six')
            if kwargs.get('tippani'):
                new_loan_application.tippani = convert_base64_to_image(kwargs.get('tippani'), 'tippani')
            if kwargs.get('voucher'):
                new_loan_application.voucher = convert_base64_to_image(kwargs.get('voucher'), 'voucher')
            if kwargs.get('debit_credit'):
                new_loan_application.debit_credit = convert_base64_to_image(kwargs.get('debit_credit'), 'debit_credit')
            if kwargs.get('quotation'):
                new_loan_application.quotation = convert_base64_to_image(kwargs.get('quotation'), 'quotation')
            if kwargs.get('memo'):
                new_loan_application.memo = convert_base64_to_image(kwargs.get('memo'), 'memo')
            if kwargs.get('credit_note'):
                new_loan_application.credit_note = convert_base64_to_image(kwargs.get('credit_note'), 'credit_note')
            if kwargs.get('service_break'):
                new_loan_application.service_break = kwargs.get('service_break')
            if kwargs.get('vacations'):
                new_loan_application.vacations = kwargs.get('vacations')
            if kwargs.get('comment'):
                new_loan_application.comment = kwargs.get('comment')

            new_loan_application.save()

            return CreateLoanApplication(
                loan_application=new_loan_application,
                status=200,
                msg='Created Successfully'
            )

        except Exception as e:
            return CreateLoanApplication(
                status=500,
                msg='{}'.format(e)
            )


class EditLoanApplication(graphene.Mutation):
    loan_application = graphene.Field(LoanApplicationType)
    status = graphene.Int()
    msg = graphene.String()

    class Arguments:
        loan_application_id = graphene.Int(required=True)
        loan_type = graphene.Int()
        loan_amount = graphene.Float()
        loan_for = graphene.String()
        status = graphene.String()
        loan_utilization_place = graphene.String()
        submission_form = graphene.String()
        lalpurja = graphene.String()
        malpot_receipt_1 = graphene.String()
        malpot_receipt_2 = graphene.String()
        verified_map = graphene.String()
        blueprint = graphene.String()
        committee_sifaris = graphene.String()
        marriage_certificate = graphene.String()
        mun_vdc_sifaris = graphene.String()
        dristibhandha = graphene.String()
        close_house_photo = graphene.String()
        inspection_report = graphene.String()
        anusuchi_six = graphene.String()
        tippani = graphene.String()
        voucher = graphene.String()
        debit_credit = graphene.String()
        quotation = graphene.String()
        memo = graphene.String()
        credit_note = graphene.String()
        approved_letter = graphene.String()
        full_name = graphene.String()
        employee_id = graphene.String()
        permanent_address = graphene.String()
        temporary_address = graphene.String()
        dob = graphene.String()
        recruit_date = graphene.String()
        recruit_position = graphene.String()
        recruit_level = graphene.String()
        current_position = graphene.String()
        current_level = graphene.String()
        current_branch = graphene.String()
        citizenship_no = graphene.String()
        citizenship_issued_place = graphene.String()
        citizenship_issued_date = graphene.String()
        service_break = graphene.String()
        retirement_date = graphene.String()
        vacations = graphene.String()
        comment = graphene.String()

    # @user_passes_test(lambda user: check_user_permissions(user, 'loan', 'update'))
    def mutate(self, info, *args, **kwargs):
        try:
            loan_application = LoanApplication.objects.get(pk=kwargs.get('loan_application_id'))

            if kwargs.get('loan_type'):
                loan_class = LoanType.objects.get(pk=kwargs.get('loan_type'))
                loan_application.loan_type = loan_class
            if kwargs.get('loan_amount'):
                loan_application.loan_amount = kwargs.get('loan_amount')
            if kwargs.get('loan_for'):
                loan_application.loan_for = kwargs.get('loan_for')
            if kwargs.get('loan_utilization_place'):
                loan_application.loan_utilization_place = kwargs.get('loan_utilization_place')
            if kwargs.get('submission_form'):
                loan_application.submission_form = convert_base64_to_image(kwargs.get('submission_form'), 'submission_form')
            if kwargs.get('lalpurja'):
                loan_application.lalpurja = convert_base64_to_image(kwargs.get('lalpurja'), 'lalpurja')
            if kwargs.get('malpot_receipt_1'):
                loan_application.malpot_receipt_1 = convert_base64_to_image(kwargs.get('malpot_receipt_1'), 'malpot_receipt_1')
            if kwargs.get('malpot_receipt_2'):
                loan_application.malpot_receipt_2 = convert_base64_to_image(kwargs.get('malpot_receipt_2'), 'malpot_receipt_2')
            if kwargs.get('verified_map'):
                loan_application.verified_map = convert_base64_to_image(kwargs.get('verified_map'), 'verified_map')
            if kwargs.get('blueprint'):
                loan_application.blueprint = convert_base64_to_image(kwargs.get('blueprint'), 'blueprint')
            if kwargs.get('committee_sifaris'):
                loan_application.committee_sifaris = convert_base64_to_image(kwargs.get('committee_sifaris'), 'committee_sifaris')
            if kwargs.get('marriage_certificate'):
                loan_application.marriage_certificate = convert_base64_to_image(kwargs.get('marriage_certificate'),
                                                                    'marriage_certificate')
            if kwargs.get('mun_vdc_sifaris'):
                loan_application.mun_vdc_sifaris = convert_base64_to_image(kwargs.get('mun_vdc_sifaris'), 'mun_vdc_sifaris')
            if kwargs.get('dristibhandha'):
                loan_application.dristibhandha = convert_base64_to_image(kwargs.get('dristibhandha'), 'dristibhandha')
            if kwargs.get('close_house_photo'):
                loan_application.close_house_photo = convert_base64_to_image(kwargs.get('close_house_photo'), 'close_house_photo')
            if kwargs.get('inspection_report'):
                loan_application.inspection_report = convert_base64_to_image(kwargs.get('inspection_report'), 'inspection_report')
            if kwargs.get('anusuchi_six'):
                loan_application.anusuchi_six = convert_base64_to_image(kwargs.get('anusuchi_six'), 'anusuchi_six')
            if kwargs.get('tippani'):
                loan_application.tippani = convert_base64_to_image(kwargs.get('tippani'), 'tippani')
            if kwargs.get('voucher'):
                loan_application.voucher = convert_base64_to_image(kwargs.get('voucher'), 'voucher')
            if kwargs.get('debit_credit'):
                loan_application.debit_credit = convert_base64_to_image(kwargs.get('debit_credit'), 'debit_credit')
            if kwargs.get('quotation'):
                loan_application.quotation = convert_base64_to_image(kwargs.get('quotation'), 'quotation')
            if kwargs.get('memo'):
                loan_application.memo = convert_base64_to_image(kwargs.get('memo'), 'memo')
            if kwargs.get('credit_note'):
                loan_application.credit_note = convert_base64_to_image(kwargs.get('credit_note'), 'credit_note')
            if kwargs.get('approved_letter'):
                loan_application.approved_letter = convert_base64_to_image(kwargs.get('approved_letter'), 'approved_letter')
            if kwargs.get('full_name'):
                loan_application.full_name = kwargs.get('full_name')
            if kwargs.get('employee_id'):
                loan_application.employee_id = kwargs.get('employee_id')
            if kwargs.get('permanent_address'):
                loan_application.permanent_address = kwargs.get('permanent_address')
            if kwargs.get('temporary_address'):
                loan_application.temporary_address = kwargs.get('temporary_address')
            if kwargs.get('dob'):
                loan_application.dob = kwargs.get('dob')
            if kwargs.get('recruit_date'):
                loan_application.recruit_date = kwargs.get('recruit_date')
            if kwargs.get('recruit_position'):
                loan_application.recruit_position = kwargs.get('recruit_position')
            if kwargs.get('recruit_level'):
                loan_application.recruit_level = kwargs.get('recruit_level')
            if kwargs.get('current_position'):
                loan_application.current_position = kwargs.get('current_position')
            if kwargs.get('current_level'):
                loan_application.current_level = kwargs.get('current_level')
            if kwargs.get('citizenship_no'):
                loan_application.citizenship_no = kwargs.get('citizenship_no')
            if kwargs.get('citizenship_issued_place'):
                loan_application.citizenship_issued_place = kwargs.get('citizenship_issued_place')
            if kwargs.get('citizenship_issued_date'):
                loan_application.citizenship_issued_date = kwargs.get('citizenship_issued_date')
            if kwargs.get('service_break'):
                loan_application.service_break = kwargs.get('service_break')
            if kwargs.get('retirement_date'):
                loan_application.retirement_date = kwargs.get('retirement_date')
            if kwargs.get('vacations'):
                loan_application.vacations = kwargs.get('vacations')
            if kwargs.get('status'):
                loan_application.status = kwargs.get('status')
            if kwargs.get('current_branch'):
                loan_application.current_branch = kwargs.get('current_branch')
            if kwargs.get('comment'):
                loan_application.comment = kwargs.get('comment')

            loan_application.save()

            return EditLoanApplication(
                loan_application=loan_application,
                msg='Updated Successfully',
                status=200
            )
        except ObjectDoesNotExist:
            return EditLoanApplication(
                status=404,
                msg='Loan not found'
            )
        except Exception as e:
            return EditLoanApplication(
                status=500,
                msg='Something went wrong'
            )


class DeleteLoanApplication(graphene.Mutation):
    msg = graphene.String()
    status = graphene.Int()

    class Arguments:
        loan_application_id = graphene.Int(required=True)

    @user_passes_test(lambda user: check_user_permissions(user, 'loan', 'delete'))
    def mutate(self, info, *args, **kwargs):
        try:
            loan_application = LoanApplication.objects.get(pk=kwargs.get('loan_application_id'))

            loan_application.delete()

            return DeleteLoanApplication(
                msg='Deleted Successfully',
                status=400,
            )
        except ObjectDoesNotExist:
            return DeleteLoanApplication(
                msg='Loan does not exists',
                status=404,
            )
        except Exception as e:
            return DeleteLoanApplication(
                msg='Something went wrong',
                status=500,
            )


class Mutation(graphene.ObjectType):
    create_loan_application = CreateLoanApplication.Field()
    edit_loan_application = EditLoanApplication.Field()
    delete_loan_application = DeleteLoanApplication.Field()