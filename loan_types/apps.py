from django.apps import AppConfig


class LoanTypesConfig(AppConfig):
    name = 'loan_types'
