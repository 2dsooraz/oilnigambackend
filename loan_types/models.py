from django.db import models


# Create your models here.

class LoanType(models.Model):
    name = models.CharField(max_length=30)
    period_years = models.FloatField()
    interest_rate = models.FloatField()
    num_payments_per_year = models.IntegerField()

    def __str__(self):
        return '{}'.format(self.name)

