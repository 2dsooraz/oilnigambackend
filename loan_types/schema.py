import graphene
from graphene_django import DjangoObjectType
from django.db.models import Q
from django.core.exceptions import ObjectDoesNotExist
from graphql_jwt.decorators import user_passes_test
from .models import LoanType
from accounts.models import Account
from backend.utils import *


class LoanClassType(DjangoObjectType):
    class Meta:
        model = LoanType


class LoanClassPageType(graphene.ObjectType):
    data = graphene.List(LoanClassType)
    rows = graphene.Int()
    rows_count = graphene.Int()
    page = graphene.Int()
    pages = graphene.Int()

class Query(graphene.ObjectType):
    all_loan_types = graphene.Field(LoanClassPageType,
                                    search=graphene.String(),
                                    page=graphene.Int(),
                                    rows=graphene.Int(),
                                    order=graphene.String(description="Allowed Values: 'pk', '-pk', 'name', '-name' etc", default_value="")
                                    )

    loan_type = graphene.Field(LoanClassType,
                               loan_type_id=graphene.Int()
                               )

    @user_passes_test(lambda user: check_user_permissions(user, 'loan', 'retrieve'))
    def resolve_all_loan_types(self, info, page=1, rows=10, *args, **kwargs):
        filters = Q()

        if kwargs.get('search'):
            filters = Q(name__icontains=kwargs.get('search'))

        data, rows_count, rows, pages, page = paginate(LoanType, page, rows, filters, order=kwargs.get('order'))

        return LoanClassPageType(
            data=data,
            rows=rows,
            rows_count=rows_count,
            page=page,
            pages=pages,
        )


    @user_passes_test(lambda user: check_user_permissions(user, 'loan', 'retrieve'))
    def resolve_loan_type(self, info, **kwargs):
        if kwargs.get('loan_type_id'):
            return LoanType.objects.get(pk=kwargs.get('loan_type_id'))
        return None


class CreateLoanType(graphene.Mutation):
    loan_type = graphene.Field(LoanClassType)
    status = graphene.Int()
    msg = graphene.String()

    class Arguments:
        name = graphene.String(required=True)
        period_years = graphene.Float(required=True)
        interest_rate = graphene.Float(required=True)
        num_payments_per_year = graphene.String(required=True)

    @user_passes_test(lambda user: check_user_permissions(user, 'loan', 'create'))
    def mutate(self, info, *args, **kwargs):
        try:
            new_loan_type = LoanType.objects.create(
                name=kwargs.get('name'),
                period_years=kwargs.get('period_years'),
                interest_rate=kwargs.get('interest_rate'),
                num_payments_per_year=kwargs.get('num_payments_per_year')
            )

            new_loan_type.save()

            return CreateLoanType(
                loan_type=new_loan_type,
                status=201,
                msg='Created Successfully'
            )
        except Exception as e:
            return CreateLoanType(
                status=500,
                msg='Something went wrong'
            )


class EditLoanType(graphene.Mutation):
    loan_type = graphene.Field(LoanClassType)
    status = graphene.Int()
    msg = graphene.String()

    class Arguments:
        loan_type_id = graphene.Int(required=True)
        name = graphene.String()
        period_years = graphene.Float()
        interest_rate = graphene.Float()
        num_payments_per_year = graphene.Int()

    @user_passes_test(lambda user: check_user_permissions(user, 'loan', 'update'))
    def mutate(self, info, *args, **kwargs):
        try:
            loan_type = LoanType.objects.get(pk=kwargs.get('loan_type_id'))

            if kwargs.get('name'):
                loan_type.name = kwargs.get('name')
            if kwargs.get('period_years'):
                loan_type.period_years = kwargs.get('period_years')
            if kwargs.get('interest_rate'):
                loan_type.interest_rate = kwargs.get('interest_rate')
            if kwargs.get('num_payments_per_year'):
                loan_type.num_payments_per_year = kwargs.get('num_payments_per_year')
            loan_type.save()

            return EditLoanType(
                loan_type=loan_type,
                status=200,
                msg='Updated Successfully'
            )
        except ObjectDoesNotExist:
            return EditLoanType(
                status=404,
                msg='Not Found'
            )
        except Exception as e:
            return EditLoanType(
                status=500,
                msg='Something went wrong'
            )


class DeleteLoanType(graphene.Mutation):
    msg = graphene.String()
    status= graphene.Int()

    class Arguments:
        loan_type_id = graphene.Int(required=True)

    @user_passes_test(lambda user: check_user_permissions(user, 'loan', 'delete'))
    def mutate(self, info, *args, **kwargs):
        try:
            loan_type = LoanType.objects.get(pk=kwargs.get('loan_type_id'))

            loan_type.delete()

            return DeleteLoanType(
                msg='Successfully deleted',
                status=400
            )
        except ObjectDoesNotExist:
            return DeleteLoanType(
                msg='Not Found',
                status=404
            )
        except Exception as e:
            return DeleteLoanType(
                msg='Something went wrong',
                status=500
            )


class Mutation(graphene.ObjectType):
    create_loan_type = CreateLoanType.Field()
    edit_loan_type = EditLoanType.Field()
    delete_loan_type = DeleteLoanType.Field()
