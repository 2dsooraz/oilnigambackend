from django.db import models
from django.db.models.signals import pre_save

from accounts.models import Account
from backend.helpers import *
from loan_types.models import LoanType

# Create your models here.


def get_loan_assets_path(instance, filename):
    return 'loans/{}/{}'.format(instance.pk, filename)


class Loan(models.Model):

    loan_type = models.ForeignKey(LoanType, on_delete=models.DO_NOTHING)
    loan_amount = models.FloatField(blank=False, null=False)
    loan_for = models.CharField(max_length=100)
    loan_utilization_place = models.CharField(max_length=100)
    full_name = models.CharField(max_length=100)
    is_active = models.BooleanField(default=True)
    permanent_address = models.CharField(max_length=100)
    temporary_address = models.CharField(max_length=100)
    dob = models.CharField(max_length=10)
    recruit_date = models.CharField(max_length=10)
    recruit_position = models.CharField(max_length=10)
    recruit_level = models.CharField(max_length=100)
    current_position = models.CharField(max_length=100)
    current_level = models.CharField(max_length=100)
    current_branch = models.CharField(max_length=100)
    citizenship_no = models.CharField(max_length=30)
    citizenship_issued_place = models.CharField(max_length=100)
    citizenship_issued_date = models.CharField(max_length=10)
    service_break = models.CharField(max_length=100, null=True, blank=True)
    retirement_date = models.CharField(max_length=10)
    vacations = models.CharField(max_length=100, null=True, blank=True)
    employee_id = models.CharField(max_length=50)
    loan_commence_date = models.CharField(max_length=10, blank=True, null=True)
    created_by = models.ForeignKey(Account, on_delete=models.DO_NOTHING)
    comment = models.TextField(null=True, blank=True)
    created_at = models.CharField(blank=True, null=True, max_length=10)
    updated_at = models.CharField(blank=True, null=True, max_length=10)

    def __str__(self):
        return "{}'s {} loan".format(self.full_name, self.loan_type)


def pre_save_change(sender, instance, *args, **kwargs):
    # checking the value of created-at
    # if the value is true, it means the instance is getting modified,
    # so, we don't need to handle created_at field.
    if not instance.created_at:
        instance.created_at = english_to_nepali()

    instance.updated_at = english_to_nepali()


pre_save.connect(pre_save_change, Loan)