import graphene
from graphene_django import DjangoObjectType

from django.db.models import Q
from django.core.exceptions import ObjectDoesNotExist
from graphql_jwt.decorators import user_passes_test

from .models import Loan
from loan_types.models import LoanType as LT
from accounts.models import Account
from backend.utils import *


class LoanType(DjangoObjectType):
    class Meta:
        model = Loan


class LoanPageType(graphene.ObjectType):
    data = graphene.List(LoanType)
    rows = graphene.Int()
    rows_count = graphene.Int()
    page = graphene.Int()
    pages = graphene.Int()


class Query(graphene.ObjectType):
    all_loans = graphene.Field(LoanPageType,
                               search=graphene.String(),
                               emp_id=graphene.String(),
                               page=graphene.Int(),
                               rows=graphene.Int(),
                               order=graphene.String(description="Allowed Values: 'pk', '-pk', etc", default_value=""))
    loan = graphene.Field(LoanType,
                          loan_id=graphene.Int()
                          )


    @user_passes_test(lambda user: check_user_permissions(user, 'loan', 'retrieve'))
    def resolve_all_loans(self, info, page=1, rows=10, *args, **kwargs):
        filters = Q()

        if kwargs.get('search'):
            filters = Q(loan_type__name__icontains=kwargs.get('search')) | Q(permanent_address__icontains=kwargs.get('search')) | Q(temporary_address__icontains=kwargs.get('search')) | Q(current_position__icontains=kwargs.get('search'))
        if kwargs.get('emp_id'):
            filters = filters & Q(employee_id__icontains=kwargs.get('emp_id'))

        data, rows_count, rows, pages, page = paginate(Loan, page, rows, filters, order=kwargs.get('order'))
        return LoanPageType(
            data=data,
            rows=rows,
            rows_count=rows_count,
            page=page,
            pages=pages,
        )

    @user_passes_test(lambda user: check_user_permissions(user, 'loan', 'retrieve'))
    def resolve_loan(self, info, **kwargs):
        if kwargs.get('loan_id'):
            return Loan.objects.get(pk=kwargs.get('loan_id'))
        return None

class CreateLoan(graphene.Mutation):
    loan = graphene.Field(LoanType)
    status = graphene.Int()
    msg = graphene.String()

    class Arguments:
        loan_type = graphene.Int(required=True)
        loan_amount = graphene.Float(required=True)
        loan_for = graphene.String(required=True)
        is_active = graphene.Boolean(default_value=True)
        loan_utilization_place = graphene.String(required=True)
        full_name = graphene.String(required=True)
        permanent_address = graphene.String(required=True)
        temporary_address = graphene.String(required=True)
        dob = graphene.String(required=True)
        recruit_date = graphene.String(required=True)
        recruit_position = graphene.String(required=True)
        recruit_level = graphene.String(required=True)
        current_position = graphene.String(required=True)
        current_level = graphene.String(required=True)
        current_branch = graphene.String(required=True)
        citizenship_no = graphene.String(required=True)
        citizenship_issued_place = graphene.String(required=True)
        citizenship_issued_date = graphene.String(required=True)
        service_break = graphene.String(required=True)
        retirement_date = graphene.String()
        vacations = graphene.String()
        employee_id = graphene.String(required=True)
        loan_commence_date = graphene.String(required=True)
        created_by = graphene.Int(required=True)
        comment = graphene.String()

    @user_passes_test(lambda user: check_user_permissions(user, 'loan', 'create'))
    def mutate(self, info, *args, **kwargs):
        try:
            account = Account.objects.get(pk=kwargs.get('created_by'))
            loan_class = LT.objects.get(pk=kwargs.get('loan_type'))
            new_loan = Loan.objects.create(
                created_by=account,
                loan_type=loan_class,
                loan_amount=kwargs.get('loan_amount'),
                loan_for=kwargs.get('loan_for'),
                is_active=kwargs.get('is_active'),
                loan_utilization_place=kwargs.get('loan_utilization_place'),
                full_name=kwargs.get('full_name'),
                permanent_address=kwargs.get('permanent_address'),
                temporary_address=kwargs.get('temporary_address'),
                dob=kwargs.get('dob'),
                recruit_date=kwargs.get('recruit_date'),
                recruit_position=kwargs.get('recruit_position'),
                recruit_level=kwargs.get('recruit_level'),
                current_position=kwargs.get('current_position'),
                current_level=kwargs.get('current_level'),
                current_branch=kwargs.get('current_branch'),
                citizenship_no=kwargs.get('citizenship_no'),
                citizenship_issued_place=kwargs.get('citizenship_issued_place'),
                citizenship_issued_date=kwargs.get('citizenship_issued_date'),
                retirement_date=kwargs.get('retirement_date'),
                employee_id=kwargs.get('employee_id'),
            )

            if kwargs.get('service_break'):
                new_loan.service_break = kwargs.get('service_break')
            if kwargs.get('vacations'):
                new_loan.vacations = kwargs.get('vacations')
            if kwargs.get('loan_commence_date'):
                new_loan.loan_commence_date = kwargs.get('loan_commence_date')
            if kwargs.get('comment'):
                new_loan.comment = kwargs.get('comment')

            new_loan.save()

            return CreateLoan(
                loan=new_loan,
                status=200,
                msg='Created Successfully'
            )

        except Exception as e:
            return CreateLoan(
                status=500,
                msg='{}'.format(e)
            )


class EditLoan(graphene.Mutation):
    loan = graphene.Field(LoanType)
    status = graphene.Int()
    msg = graphene.String()

    class Arguments:
        loan_id = graphene.Int(required=True)
        loan_type = graphene.Int()
        loan_amount = graphene.Float()
        loan_for = graphene.String()
        is_active = graphene.Boolean()
        loan_utilization_place = graphene.String()
        full_name = graphene.String()
        permanent_address = graphene.String()
        temporary_address = graphene.String()
        dob = graphene.String()
        recruit_date = graphene.String()
        recruit_position = graphene.String()
        recruit_level = graphene.String()
        current_position = graphene.String()
        current_level = graphene.String()
        current_branch = graphene.String()
        citizenship_no = graphene.String()
        citizenship_issued_place = graphene.String()
        citizenship_issued_date = graphene.String()
        service_break = graphene.String()
        retirement_date = graphene.String()
        vacations = graphene.String()
        employee_id = graphene.String()
        loan_commence_date = graphene.String()
        comment = graphene.String()

    @user_passes_test(lambda user: check_user_permissions(user, 'loan', 'update'))
    def mutate(self, info, *args, **kwargs):
        try:

            loan = Loan.objects.get(pk=kwargs.get('loan_id'))

            if kwargs.get('loan_type'):
                loan_class = LT.objects.get(pk=kwargs.get('loan_type'))
                loan.loan_type = loan_class
            if kwargs.get('loan_amount'):
                loan.loan_amount = kwargs.get('loan_amount')
            if kwargs.get('loan_for'):
                loan.loan_for = kwargs.get('loan_for')
            if kwargs.get('loan_utilization_place'):
                loan.loan_utilization_place = kwargs.get('loan_utilization_place')
            if kwargs.get('full_name'):
                loan.full_name = kwargs.get('full_name')
            if kwargs.get('is_active') is not None:
                loan.is_active = kwargs.get('is_active')
            if kwargs.get('loan_commence_date'):
                loan.loan_commence_date = kwargs.get('loan_commence_date')
            if kwargs.get('permanent_address'):
                loan.permanent_address = kwargs.get('permanent_address')
            if kwargs.get('temporary_address'):
                loan.temporary_address = kwargs.get('temporary_address')
            if kwargs.get('dob'):
                loan.dob = kwargs.get('dob')
            if kwargs.get('recruit_date'):
                loan.recruit_date = kwargs.get('recruit_date')
            if kwargs.get('recruit_position'):
                loan.recruit_position = kwargs.get('recruit_position')
            if kwargs.get('recruit_level'):
                loan.recruit_level = kwargs.get('recruit_level')
            if kwargs.get('current_position'):
                loan.current_position = kwargs.get('current_position')
            if kwargs.get('current_level'):
                loan.current_level = kwargs.get('current_level')
            if kwargs.get('current_branch'):
                loan.current_branch = kwargs.get('current_branch')
            if kwargs.get('citizenship_no'):
                loan.citizenship_no = kwargs.get('citizenship_no')
            if kwargs.get('citizenship_issued_place'):
                loan.citizenship_issued_place = kwargs.get('citizenship_issued_place')
            if kwargs.get('citizenship_issued_date'):
                loan.citizenship_issued_date = kwargs.get('citizenship_issued_date')
            if kwargs.get('service_break'):
                loan.service_break = kwargs.get('service_break')
            if kwargs.get('vacations'):
                loan.vacations = kwargs.get('vacations')
            if kwargs.get('employee_id'):
                loan.employee_id = kwargs.get('employee_id')
            if kwargs.get('comment'):
                loan.comment = kwargs.get('comment')

            loan.save()

            return EditLoan(
                loan=loan,
                msg='Updated Successfully',
                status=200
            )
        except ObjectDoesNotExist:
            return EditLoan(
                status=404,
                msg='Loan not found'
            )
        except Exception as e:
            return EditLoan(
                status=500,
                msg='Something went wrong'
            )


class DeleteLoan(graphene.Mutation):
    msg = graphene.String()
    status = graphene.Int()

    class Arguments:
        loan_id = graphene.Int(required=True)

    @user_passes_test(lambda user: check_user_permissions(user, 'loan', 'delete'))
    def mutate(self, info, *args, **kwargs):
        try:
            loan = Loan.objects.get(pk=kwargs.get('loan_id'))

            loan.delete()

            return DeleteLoan(
                msg='Deleted Successfully',
                status=400,
            )
        except ObjectDoesNotExist:
            return DeleteLoan(
                msg='Loan does not exists',
                status=404,
            )
        except Exception as e:
            return DeleteLoan(
                msg='Something went wrong',
                status=500,
            )


class Mutation(graphene.ObjectType):
    create_loan = CreateLoan.Field()
    edit_loan = EditLoan.Field()
    delete_loan = DeleteLoan.Field()

