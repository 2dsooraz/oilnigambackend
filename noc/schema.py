import datetime

import graphene
import accounts.schema
# import backend.schema
import loans.schema
import loan_types.schema
import transactions.schema
import loan_applications.schema
import graphql_jwt


class Query(
    accounts.schema.Query,
    loans.schema.Query,
    loan_types.schema.Query,
    transactions.schema.Query,
    loan_applications.schema.Query,
    graphene.ObjectType,
):
    pass


class Mutation(
    accounts.schema.Mutation,
    loans.schema.Mutation,
    loan_types.schema.Mutation,
    transactions.schema.Mutation,
    loan_applications.schema.Mutation,
    graphene.ObjectType,
):
    token_auth = graphql_jwt.ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()


schema = graphene.Schema(query=Query, mutation=Mutation)

GRAPHQL_JWT = {
    'JWT_VERIFY_EXPIRATION': True,
    'JWT_EXPIRATION_DELTA': datetime.timedelta(days=1),
    'JWT_REFRESH_EXPIRATION_DELTA': datetime.timedelta(days=7),
}
