from django.db import models
from django.db.models.signals import pre_save

from backend.helpers import *
from loans.models import Loan
from accounts.models import Account


# Create your models here.
class Transaction(models.Model):
    incoming = 'CREDIT'
    outgoing = 'DEBIT'

    TRANSACTION_TYPE_CHOICES = (
        (incoming, incoming),
        (outgoing, outgoing),
    )
    transaction_of = models.ForeignKey(Loan, on_delete=models.DO_NOTHING)
    transaction_amount = models.FloatField()
    principle_amount = models.FloatField(blank=True, null=True)
    interest_amount = models.FloatField(blank=True, null=True)
    extra_payment = models.FloatField(blank=True, null=True)
    transaction_date = models.CharField(max_length=10, null=True, blank=True)
    transaction_type = models.CharField(max_length=30, choices=TRANSACTION_TYPE_CHOICES)
    created_by = models.ForeignKey(Account, on_delete=models.DO_NOTHING)
    created_at = models.CharField(max_length=10, null=True, blank=True)
    updated_at = models.CharField(max_length=10, null=True, blank=True)

    def __str__(self):
        return "{} transaction of {} value {}".format(self.transaction_type, self.transaction_of.full_name, self.transaction_amount)


def pre_save_change(sender, instance, *args, **kwargs):
    # checking the value of created-at
    # if the value is true, it means the instance is getting modified,
    # so, we don't need to handle created_at field.
    if not instance.created_at:
        instance.created_at = english_to_nepali()

    instance.updated_at = english_to_nepali()


pre_save.connect(pre_save_change, Transaction)
