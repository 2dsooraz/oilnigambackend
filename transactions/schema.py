import graphene

from graphene_django import DjangoObjectType

from django.db.models import Q
from django.core.exceptions import ObjectDoesNotExist
from graphql_jwt.decorators import user_passes_test

from accounts.models import Account
from loans.models import Loan
from .models import Transaction

from backend.utils import *


class TransactionType(DjangoObjectType):
    class Meta:
        model = Transaction


class TransactionPageType(graphene.ObjectType):
    data = graphene.List(TransactionType)
    rows = graphene.Int()
    rows_count = graphene.Int()
    page = graphene.Int()
    pages = graphene.Int()


class Query(graphene.ObjectType):
    all_transactions = graphene.Field(TransactionPageType,
                                  search = graphene.String(),
                                  page=graphene.Int(),
                                  rows=graphene.Int(),
                                  order=graphene.String(description="Allowed Values: 'pk', '-pk', etc", default_value=""))
    transaction = graphene.Field(TransactionType,
                             transaction_id=graphene.Int()
                             )

    @user_passes_test(lambda user: check_user_permissions(user, 'transaction', 'retrieve'))
    def resolve_all_transactions(self, info, page=1, rows=10, *args, **kwargs):
        filters = Q()

        if kwargs.get('search'):
            # :TODO: if possible handle search parameters here
            filters = Q(transaction_type__icontains=kwargs.get('search'))

        data, rows_count, rows, pages, page = paginate(Transaction, page, rows, filters, order=kwargs.get('order'))
        return TransactionPageType(
            data=data,
            rows=rows,
            rows_count=rows_count,
            page=page,
            pages=pages,
        )

    @user_passes_test(lambda user: check_user_permissions(user, 'transaction', 'retrieve'))
    def resolve_transaction(self, info, *args, **kwargs):
        if kwargs.get('transaction_id'):
            return Transaction.objects.get(pk=kwargs.get('transaction_id'))

        return None


class CreateTransaction(graphene.Mutation):
    transaction = graphene.Field(TransactionType)
    status = graphene.Int()
    msg = graphene.String()

    class Arguments:
        transaction_of = graphene.Int(required=True)
        transaction_amount = graphene.Float(required=True)
        principle_amount = graphene.Float()
        interest_amount = graphene.Float()
        extra_payment = graphene.Float()
        transaction_date = graphene.String()
        transaction_type = graphene.String(required=True)
        created_by = graphene.Int(required=True)

    @user_passes_test(lambda user: check_user_permissions(user, 'transaction', 'create'))
    def mutate(self, info, *args, **kwargs):
        try:
            loan = Loan.objects.get(pk=kwargs.get('transaction_of'))
            account = Account.objects.get(pk=kwargs.get('created_by'))

            new_transaction = Transaction.objects.create(
                transaction_of=loan,
                created_by=account,
                principle_amount=kwargs.get('principle_amount'),
                interest_amount=kwargs.get('interest_amount'),
                transaction_amount=kwargs.get('transaction_amount'),
                extra_payment=kwargs.get('extra_payment'),
                transaction_date=kwargs.get('transaction_date'),
                transaction_type=kwargs.get('transaction_type'),
            )

            new_transaction.save()

            return CreateTransaction(
                transaction=new_transaction,
                msg='Successfully created',
                status=201
            )
        except ObjectDoesNotExist:
            return CreateTransaction(
                status=404,
                msg='Loan not found'
            )
        except Exception as e:
            return CreateTransaction(
                status=500,
                msg='Something went wrong'
            )


class EditTransaction(graphene.Mutation):
    transaction = graphene.Field(TransactionType)
    status = graphene.Int()
    msg = graphene.String()

    class Arguments:
        transaction_id = graphene.Int(required=True)
        created_by = graphene.Int()
        transaction_of = graphene.Int()
        transaction_amount = graphene.Float()
        principle_amount = graphene.Float()
        interest_amount = graphene.Float()
        extra_payment = graphene.Float()
        transaction_date = graphene.String()
        transaction_type = graphene.String()

    @user_passes_test(lambda user: check_user_permissions(user, 'transaction', 'update'))
    def mutate(self, info, *args, **kwargs):
        try:
            transaction = Transaction.objects.get(pk=kwargs.get('transaction_id'))

            if kwargs.get('transaction_of'):
                loan = Loan.objects.get(pk=kwargs.get('transaction_of'))
                transaction.transaction_of = loan

            if kwargs.get('created_by'):
                account = Account.objects.get(pk=kwargs.get('created_by'))
                transaction.created_by = account

            if kwargs.get('transaction_type'):
                transaction.transaction_type = kwargs.get('transaction_type'),

            if kwargs.get('transaction_amount'):
                transaction.transaction_amount = kwargs.get('transaction_amount')

            if kwargs.get('extra_payment'):
                transaction.extra_payment = kwargs.get('extra_payment')
            if kwargs.get('transaction_date'):
                transaction.transaction_date = kwargs.get('transaction_date')

            if kwargs.get('principle_amount'):
                transaction.principle_amount = kwargs.get('principle_amount')

            if kwargs.get('interest_amount'):
                transaction.interest_amount = kwargs.get('interest_amount')

            transaction.save()

            return EditTransaction(
                transaction=transaction,
                status=200,
                msg='Updated Successfully'
            )
        except ObjectDoesNotExist:
            return EditTransaction(
                status=404,
                msg='Transaction or Loan not found'
            )
        except Exception as e:
            return EditTransaction(
                status=500,
                msg='Something went wrong'
            )


class DeleteTransaction(graphene.Mutation):
    status = graphene.Int()
    msg = graphene.String()

    class Arguments:
        transaction_id = graphene.Int(required=True)

    @user_passes_test(lambda user: check_user_permissions(user, 'transaction', 'delete'))
    def mutate(self, info, *args, **kwargs):
        try:
            transaction = Transaction.objects.get(pk=kwargs.get('transaction_id'))

            transaction.delete()

            return DeleteTransaction(
                msg='Successfully deleted',
                status=400,
            )
        except ObjectDoesNotExist:
            return DeleteTransaction(
                msg='Transaction not found',
                status=400
            )
        except Exception as e:
            return DeleteTransaction(
                status=500,
                msg='Something went wrong'
            )


class Mutation(graphene.ObjectType):
    create_transaction = CreateTransaction.Field()
    edit_transaction = EditTransaction.Field()
    delete_transaction = DeleteTransaction.Field()
